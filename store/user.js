export const state = () => ({
  authToken: null
})

export const getters = {
  authToken: (state) => state.authToken
}

export const mutations = {
  setAuthToken: (state, token) => (state.authToken = token)
}
